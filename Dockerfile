FROM golang:1.13-alpine AS builder

WORKDIR /app

COPY main.go go.mod ./

RUN CGO_ENABLED=0 go build -o web

FROM scratch

WORKDIR /app

COPY --from=builder /app/web ./

COPY static/ ./static

EXPOSE 5000

ENTRYPOINT ["/app/web"]
