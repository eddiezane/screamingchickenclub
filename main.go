package main

import (
	"log"
	"net/http"
	"os"
)

func main() {
	port, ok := os.LookupEnv("PORT")
	if !ok {
		port = "5000"
	}
	http.Handle("/", http.FileServer(http.Dir("static")))
	log.Printf("server starting on %v", port)
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
